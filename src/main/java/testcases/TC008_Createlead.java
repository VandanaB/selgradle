package testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC008_Createlead extends ProjectMethods{
	
	

	@BeforeTest
	public void setData() {
		testCaseName = "TC008_Createlead";
		testDesc = "Create new lead";
		author = "Vandana";
		category = "smoke";
	}
	
	@Test(dataProvider="data")
	public void createLead(String comName, String fstName,String lstName,String eMail,String phNum) {
		
		click(locateElement("LinkText", "Leads"));
		click(locateElement("LinkText", "Create Lead"));
		type(locateElement("xpath","//input[@id='createLeadForm_companyName']"), comName);
		type(locateElement("createLeadForm_firstName"),fstName);
		type(locateElement("id", "createLeadForm_lastName"), lstName);
		type(locateElement("id", "createLeadForm_primaryEmail"), eMail);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ""+phNum);//to convert int to String concat with String
		click(locateElement("name", "submitButton"));
		
	}

	@DataProvider(name="data")
	
	public Object[][] fetchData() throws IOException {
		
		
	 Object[][] data = ReadExcel.readXcel("Book2");
		return data;
	 
	 /*
		data[0][0] = "TCS";
		data[0][1] = "Vandana";
		data[0][2] = "Balraj";
		data[0][3] = "vandana@gmail.com";
		data[0][4] = 1234556666;
		
		data[1][0] = "CTS";
		data[1][1] = "Vandana";
		data[1][2] = "Govindaraj";
		data[1][3] = "vandanag@gmail.com";
		data[1][4] = 1256478559;
		*/
	
	}
	
	
	
	
	
	
	
	
	
}
