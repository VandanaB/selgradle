package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC004MergeLeads extends ProjectMethods{
	
	@BeforeTest(groups= {"smoke"})
	
	public void setData1() {
		testCaseName = "TC007";
		testDesc = "Merge A Lead";
		author = "Vandana";
		category = "smoke";
	}
	@Test(dataProvider="data")
	public void MergeLead(String comName,String fName,String lName,String eMail,String num) throws InterruptedException {
			
     //merge leads page
		click(locateElement("LinkText", "Leads"));
		click(locateElement("LinkText", "Merge Leads"));
		click(locateElement("xpath", "//table[contains(@id,'widget')]/following-sibling::a/img"));
		
		switchToWindow(1);
		type(locateElement("xpath", "//input[@name='firstName']"), fName);
		type(locateElement("xpath", "//input[@name='lastName']"), lName);
		type(locateElement("xpath", "//input[@name='companyName']"), comName);
		click(locateElement("xpath", "(//button[@class='x-btn-text'])[1]"));
		
		Thread.sleep(2000);
		clickWithoutSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		
		switchToWindow(0);
		click(locateElement("xpath", "//table[@name='ComboBox_partyIdTo']/following-sibling::a/img"));
		
		switchToWindow(1);
		type(locateElement("xpath", "//input[@name='firstName']"), "Hari");
		type(locateElement("xpath", "//input[@name='lastName']"), "M");
		WebElement findLeadButton1 = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(findLeadButton1);
		Thread.sleep(2000);
		clickWithoutSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		switchToWindow(0);
		WebElement mergeButton = locateElement("LinkText", "Merge");
		clickWithoutSnap(mergeButton);
		acceptAlert();
		click(mergeButton);
	}
	@DataProvider(name="data", indices= {0})
	public Object[][] fetchData() throws IOException{
		Object[][] data = ReadExcel.readXcel("TC002");
		return data;
	}
  }

















