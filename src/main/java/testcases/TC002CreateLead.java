package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
       
public class TC002CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "Vandana";
		category = "smoke";
	}
	 
	@Test
	public void createLead() {
		
		    WebElement eleCreateLead = locateElement("LinkText", "Create Lead");
		    click(eleCreateLead);
			WebElement eleCompanyName = locateElement("xpath","//input[@id='createLeadForm_companyName']");
		    type(eleCompanyName, "TCS");
		    WebElement elefirstName = locateElement("createLeadForm_firstName");
		    type(elefirstName, "Vandana");
		    WebElement elelastName = locateElement("createLeadForm_lastName");
		    type(elelastName, "Balraj");
		    WebElement ddIndustry = locateElement("createLeadForm_industryEnumId");
		    selectDropDownUsingText(ddIndustry, "Computer Software");
		    	   
		    WebElement eleparentAcc = locateElement("xpath", "//input[@id='createLeadForm_parentPartyId']/following-sibling::a/img");
		    click(eleparentAcc);
		    switchToWindow(1);
		    WebElement eleAcc = locateElement("LinkText", "accountlimit100");
		    clickWithoutSnap(eleAcc);
		    switchToWindow(0); 
		    WebElement runCreateLead = locateElement("name", "submitButton");
		    click(runCreateLead);
		  
         }
       }