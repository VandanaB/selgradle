package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC006_Duplicate extends ProjectMethods{
	
	@BeforeTest(groups= {"sanity"})
	public void setData3() {
		testCaseName = "TC006";
		testDesc = "Duplicate A Lead";
		author = "Vandana";
		category = "sanity";
	}
	
	//@Test(groups= {"sanity"},dependsOnMethods= {"testcases.TC002CreateLead.createLead"})
	@Test(dataProvider="data",enabled=false)
	public void duplicateLead(String comName,String fName,String lName,String email,String num) {
		
		click(locateElement("LinkText", "Leads"));
	    WebElement findLeadsTab = locateElement("LinkText", "Find Leads");
	    click(findLeadsTab);
	   type(locateElement("xpath", "(//div[@tabindex='-1'])[19]//input"), fName); 
		type(locateElement("xpath", "(//div[@tabindex='-1'])[20]//input"), lName);
		type(locateElement("xpath", "(//div[@tabindex='-1'])[21]//input"), comName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
        click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
    	 WebElement leadId = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
    	 click(leadId);
		WebElement duplButton = locateElement("LinkText","Duplicate Lead");
		click(duplButton);
		WebElement crtLdButton = locateElement("name","submitButton");
		click(crtLdButton);	
	}
	
	
	@DataProvider(name="data",indices= {0})
	public Object[][] fetchData() throws IOException{
		Object[][] data=ReadExcel.readXcel("TC002");
		return data;
		
	}
	
	
	
	
	
	
	
	
	
}


