package wdMethods;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcel;



public class ProjectMethods extends SeMethods {
	public String dataSheetName;
	public static String firstResLeadID;
	
	@BeforeSuite//(groups= {"common"})
	public void beforeSuite() {
		startResult();
	}
	
	@Parameters({"url"})
	
	@BeforeMethod//(groups= {"common"})
	
	public void login(String url) {
		
		testCaseLevel();

		startApp("chrome", url);
	 
		
	}
	
	@AfterMethod//(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	
	@AfterSuite//(groups= {"common"})
	public void endTestCase() {
		endResult();
	}
	
@DataProvider(name="data")
	
	public Object[][] fetchData() throws IOException {
	
		return  ReadExcel.readXcel(dataSheetName);
}
}
