package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how = How.ID,using ="createLeadForm_companyName")WebElement eleCompanyName;
	@FindBy(how = How.ID,using ="createLeadForm_firstName")WebElement eleFirstName;
	@FindBy(how = How.ID,using ="createLeadForm_lastName")WebElement eleLastName;
	@FindBy(how = How.ID,using="createLeadForm_primaryEmail")WebElement eleEmailID;
	@FindBy(how = How.ID,using="createLeadForm_primaryPhoneNumber")WebElement elePhNo;
	@FindBy(how = How.NAME,using ="submitButton")WebElement eleclickCreateLeadButton;
	
	public CreateLeadPage enterCompanyName(String comName) {
		type(eleCompanyName, comName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String fName) {
		type(eleFirstName, fName);
		return this;
	}
	public CreateLeadPage enterLastName(String lName) {
		type(eleLastName, lName);
		return this;
	}
	
	public CreateLeadPage enterEmailID(String emailID) {
		type(eleEmailID, emailID);
		return this;
	}
	public CreateLeadPage enterPhNo(String num) {
		type(elePhNo, num);
		return this;
	}
	public ViewLeadPage clickCreateLeadButton() {
		click(eleclickCreateLeadButton);
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
  
}
