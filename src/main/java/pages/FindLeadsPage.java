package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);	
	}
	@FindBy(how=How.ID_OR_NAME,using="firstName")WebElement eleFirstName;
	@FindBy(how=How.ID_OR_NAME,using="lastName")WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME,using="companyName")WebElement eleComName;
	@FindBy(how=How.XPATH,using="(//button[@class='x-btn-text'])[1]")WebElement eleButton;
	@FindBy(how=How.XPATH,using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")
	                                    WebElement eleLead;
	
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]")WebElement elefname;
	@FindBy(how=How.XPATH,using="(//input[@name='lastName'])[3]")WebElement elelname;
	@FindBy(how=How.XPATH,using="(//input[@name='companyName'])[2]")WebElement elecomname;
	@FindBy(how=How.XPATH,using="(//button[@class='x-btn-text'])[7]")WebElement elefindLeadButton;
	@FindBy(how=How.NAME,using="id")WebElement eleID;
	
	public FindLeadsPage enterID(String firstResLeadID) {
		 firstResLeadID = eleLead.getText();
		type(eleID, firstResLeadID);
		return this;
	}
	
	public FindLeadsPage enterFirstName(String fName) {
		type(eleFirstName, fName);
		return this;
	}
	
	public FindLeadsPage enterLastName(String lName) {
		type(eleLastName, lName);
		return this;
	}
	
	public FindLeadsPage enterCompanyName(String comName) {
		type(eleComName, comName);
		return this;
	}
	
	public FindLeadsPage clickFindlead() {
		click(eleButton);
		return this;
	}
	 
	public MergeLeadPage clickLead() {
	clickWithoutSnap(eleLead);
	return new MergeLeadPage();
	}
	
	public ViewLeadPage clickLeadforView() {
		clickWithoutSnap(eleLead);
		return new ViewLeadPage();
		}
     
	public FindLeadsPage enterfNameforView(String fName) {
		type(elefname, fName);
		return this;	
	}
	
	public FindLeadsPage enterlNameforView(String lName) {
		type(elelname, lName);
		return this;	
	}
	
	public FindLeadsPage enterComNameforView(String comName) {
		type(elecomname, comName);
		return this;	
	}
	
	
	public FindLeadsPage clickFindleadforView() {
		click(elefindLeadButton);;
		return this;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
