package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how = How.LINK_TEXT,using ="Create Lead")WebElement eleclickCreateLead;
	@FindBy(how=How.LINK_TEXT,using="Merge Leads")WebElement eleclickMergeLead;
	@FindBy(how=How.LINK_TEXT,using="Find Leads")WebElement eleclickFindLeads;
	
	public CreateLeadPage clickCreateLead() {
		click(eleclickCreateLead);
		return new CreateLeadPage();
	}
	
	public MergeLeadPage clickMergeLead() {
		click(eleclickMergeLead);
		return new MergeLeadPage();
	}
    
	public FindLeadsPage clickFindLeadsPage() {
		click(eleclickFindLeads);
		return new FindLeadsPage();
	}
}
