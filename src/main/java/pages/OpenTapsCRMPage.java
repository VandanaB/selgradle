package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class OpenTapsCRMPage extends ProjectMethods {
	public OpenTapsCRMPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.ID,using="updateLeadForm_companyName")WebElement elecompanyname;
	@FindBy(how=How.NAME,using="submitButton")WebElement eleupdate;
	
	public OpenTapsCRMPage updateComName(String comName) {
		clearText(elecompanyname);
		type(elecompanyname, comName);
		return this;	
	}
	
	public ViewLeadPage clickUpdate() {
		click(eleupdate);
		return new ViewLeadPage();
		
	}

}
