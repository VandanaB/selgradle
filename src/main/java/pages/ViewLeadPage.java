package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how=How.ID, using ="viewLead_firstName_sp")WebElement eleVerifyName;
	@FindBy(how=How.LINK_TEXT, using ="Logout")WebElement eleclickLogout;
	@FindBy(how=How.LINK_TEXT,using="Edit")WebElement eleEdit;
	@FindBy(how=How.LINK_TEXT,using="Delete")WebElement eleDelete;
	
	
	public ViewLeadPage verifyName() {
		verifyDisplayed(eleVerifyName);
		return this;
	}
	
	public LoginPage clickLogOut() {
		click(eleclickLogout);
		return new LoginPage();
	}
	
	public OpenTapsCRMPage clickEdit() {
		click(eleEdit);
		return new OpenTapsCRMPage();
	}
	
	public MyLeadsPage clickDelete() {
		click(eleDelete);
		return new MyLeadsPage();
	}

}
