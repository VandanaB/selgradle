package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {
	
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how=How.XPATH,using="//table[contains(@id,'widget')]/following-sibling::a/img")WebElement eleFromimg;
	@FindBy(how=How.XPATH,using="//table[@name='ComboBox_partyIdTo']/following-sibling::a/img")WebElement eleToimg;
    @FindBy(how=How.LINK_TEXT,using="Merge")WebElement eleMergeButton;
	
	public FindLeadsPage clickFromimg () {
		click(eleFromimg);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	
	public FindLeadsPage clickToimg() {
		switchToWindow(0);
		click(eleToimg);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public MergeLeadPage clickMerge() {
		switchToWindow(0);
		click(eleMergeButton);
		return this;
	}
	public ViewLeadPage  clickalertBox() {
		acceptAlert();
		return new ViewLeadPage();
	}
	
	
	
	
}

