package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	public HomePage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how = How.LINK_TEXT,using = "CRM/SFA") WebElement eleCrmsfa;
	@FindBy(how = How.CLASS_NAME, using="decorativeSubmit")WebElement eleLogOut;
	
	
	public MyHomePage clickCrmsfa() {
		click(eleCrmsfa);
		return new MyHomePage();
	}
	
 

	public LoginPage clickLogOut() {

		click(eleLogOut);
		return new LoginPage();
	}

}
