package utils;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

/*each method is categorized by annotations since each has differnt functionality
 * this s ths superclass for ProjectMethods for report generation.
 * this is a abstract class which has abstract methods and normal methods*/

public abstract class Report {
  //declaring variables globally
	//static is for using the variable wen ever its called
	//Extent Test is the class adding logs about a test result 
	public static ExtentTest logger ;
	public String testCaseName, testDesc, category , author;
	//ExtentHtmlReporter class for generating HTML reports
	public ExtentHtmlReporter html ;
	public  static ExtentReports extent;


	//@BeforeSuite
	public void startResult() {
		html = new ExtentHtmlReporter("./reports/result.html");//creating object and giving path to save reports
		html.setAppendExisting(false);  //to save the reports for each test cases. 
		                               //if false the reports are overwritten
		//converting the readable file to editable mode ExtentHtml Reporter class is used
		extent = new ExtentReports();
		extent.attachReporter(html);
	}

   // here status is checked and passed to logger variable 
	public void reportStep(String status, String desc) {
		if (status.equalsIgnoreCase("pass")) { 
			logger.log(Status.PASS, desc);		//log method declares the status and desc	
		} else if (status.equalsIgnoreCase("fail")) {
			logger.log(Status.FAIL, desc);			
		}
	}
	//@AfterSuite
	public void endResult() {
		extent.flush(); //to execute the above prog we need flush to clear the memory
	}


	//@BeforeMethod
	public void testCaseLevel() { //this method is used to 
		                           //declare the variables in each test case seperately since values are dynamic
		logger = extent.createTest(testCaseName, testDesc);
		logger.assignAuthor(author);
		logger.assignCategory(category);		
	}





}


