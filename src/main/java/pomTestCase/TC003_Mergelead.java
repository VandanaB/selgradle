package pomTestCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.LoginPage;
import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC003_Mergelead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDesc = "Merge Leads";
		author = "Vandana";
		category = "smoke";
		
	}
	
	@Test(dataProvider="data")
	public void MergeLead(String comName, String fName,String lName, String emailID, String num ) throws InterruptedException {
		
		new LoginPage().enterUserName("DemoSalesManager").enterPassword("crmsfa").clickLogin()
		.clickCrmsfa().clickLeads().clickMergeLead()
		.clickFromimg()
	    .enterFirstName(fName).enterLastName(lName).enterCompanyName(comName).clickFindlead()
	    .clickLead()
	    .clickToimg()
	    .enterFirstName("Siva").enterLastName("S").enterCompanyName("TCS").clickFindlead().clickLead()
	    .clickMerge().clickalertBox();
	    
	    
		
		
	}

	@DataProvider(name="data",indices= {0})
	public Object[][] fetchData() throws IOException{
		return ReadExcel.readXcel("TC002");
		
	}
	
	
}
