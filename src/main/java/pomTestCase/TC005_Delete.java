package pomTestCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.LoginPage;
import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC005_Delete extends ProjectMethods {
	@BeforeTest
	public void setData() {
		 testCaseName = "TC004_DeleteLead";
		 testDesc = "Delete Leads";
			author = "Vandana";
			category = "smoke";	
	}
	
	@Test(dataProvider="data")
	public void editLead(String comName, String fName,String lName, String emailID, String num) {
		new LoginPage().enterUserName("DemoSalesManager").enterPassword("crmsfa").clickLogin()
		.clickCrmsfa().clickLeads().clickFindLeadsPage()
		.enterfNameforView(fName).enterlNameforView(lName).enterComNameforView(comName)
		.clickFindleadforView()
		.clickLeadforView().verifyName().clickDelete().clickFindLeadsPage().enterID(firstResLeadID)
		.clickFindlead();
	}


	@DataProvider(name="data", indices= {1})
	public Object[][] fetchData() throws IOException {
		
		return ReadExcel.readXcel("TC002");
		
	}
}
