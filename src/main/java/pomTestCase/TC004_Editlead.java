package pomTestCase;

import java.io.IOException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC004_Editlead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		 testCaseName = "TC004_EditLead";
		 testDesc = "Edit Leads";
			author = "Vandana";
			category = "smoke";	
	}
	
	@Test(dataProvider="data")
	public void editLead(String comName, String fName,String lName, String emailID, String num) {
		new LoginPage().enterUserName("DemoSalesManager").enterPassword("crmsfa").clickLogin()
		.clickCrmsfa().clickLeads().clickFindLeadsPage()
		.enterfNameforView(fName).enterlNameforView(lName).enterComNameforView(comName)
		.clickFindleadforView()
		.clickLeadforView().verifyName().clickEdit().updateComName("Wipro").clickUpdate();
		
		
		
	
		
	}
	
	@DataProvider(name="data", indices= {2})
	public Object[][] fetchData() throws IOException {
		
		return ReadExcel.readXcel("TC002");
		
	}

}
