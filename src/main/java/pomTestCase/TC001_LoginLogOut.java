package pomTestCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginLogOut extends ProjectMethods {
	
	
	/*we are writing a different method to declare variables 
	@BeforeTest level. this is equavilent to the method in report class 
	we cant use that method outside a main method if done then its overriding
	to avoid v create new method for initializing class level declared variables*/
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogOut";
		testDesc = "Login and LogOut";
		author = "Vandana";
		category = "smoke";
		dataSheetName = "TC001";
	}
	
	@Test(dataProvider="data")
	public void loginLogOut(String uName, String password) {
		/*To call the methods of LoginPage class, create object for that class by 
		giving "new class name" call methods by giving"."*/
		
				new LoginPage()
				.enterUserName(uName)
				.enterPassword(password)
				.clickLogin()
				.clickLogOut();
				
		
		
	}

}
