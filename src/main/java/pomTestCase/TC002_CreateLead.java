package pomTestCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "Create Lead";
		testDesc = "Create new Lead";
		author = "Vandana";
		category = "smoke";
		dataSheetName = "TC002";
	}
		
	
	@Test(dataProvider ="data")
	
public void CreateLead(String comName, String fName,String lName,
		                  String emailID, String num ) {
		
		
			new LoginPage()
			.enterUserName("DemoSalesManager")
			.enterPassword("crmsfa")
			.clickLogin()
			.clickCrmsfa().clickLeads().clickCreateLead().enterCompanyName(comName)
			.enterFirstName(fName).enterLastName(lName).enterEmailID(emailID).enterPhNo(num)
			.clickCreateLeadButton().verifyName();
		
		
		
	}

}
